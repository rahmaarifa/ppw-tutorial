from django.shortcuts import render
import datetime

# Enter your name here
mhs_name = 'Putri Rahma Arifa' # TODO Implement this

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age (1998)}
    return render(request, 'index.html', response)

# TODO Implement this to complete last checklist
def calculate_age(birth_year):
   today = datetime.datetime.today().year
   age = today - birth_year
   return age
